package de.schummar.parallel;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class Worker
{
	public Runnable runnable(final Window window, final boolean interruptable)
	{
		return new Runnable()
		{
			@Override public void run()
			{
				Worker.this.run(window, interruptable);
			}
		};
	}


	public ActionListener actionListener(final Window window, final boolean interruptable)
	{
		return new ActionListener()
		{
			@Override public void actionPerformed(ActionEvent e)
			{
				run(window, interruptable);
			}
		};
	}


	public void run(Window window, boolean interruptable)
	{
		new WorkerThread(this, window, interruptable);
	}


	public void runSync(Progress progress) throws InterruptedException
	{
		work(progress);
	}


	protected abstract void work(Progress progress) throws InterruptedException;
}
