package de.schummar.parallel;

public class ObservableCounter
{
	private int currentCounter;


	public ObservableCounter(int initialCounter)
	{
		currentCounter = initialCounter;
	}


	public synchronized int increment()
	{
		currentCounter++;
		notifyAll();
		return currentCounter;
	}


	public synchronized int decrement()
	{
		currentCounter--;
		notifyAll();
		return currentCounter;
	}


	public synchronized void await(int finalCounter) throws InterruptedException
	{
		while (currentCounter != finalCounter)
			wait();
	}
}
