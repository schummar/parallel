package de.schummar.parallel;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

public class WorkerDialog extends JDialog
{
	private static final long serialVersionUID = 1L;

	private JLabel label;
	private final JProgressBar bar;


	public WorkerDialog(Window window, boolean interruptable, ActionListener interruptListener)
	{
		super(window);

		JPanel labelP = new JPanel(new FlowLayout(FlowLayout.CENTER));
		add(labelP, BorderLayout.NORTH);

		labelP.add(label = new JLabel("..."), BorderLayout.NORTH);
		label.setFont(label.getFont().deriveFont(Font.BOLD, 20));
		label.setBorder(new EmptyBorder(10, 10, 10, 10));

		add(bar = new JProgressBar());
		bar.setIndeterminate(true);
		bar.setPreferredSize(new Dimension(500, bar.getPreferredSize().height));

		if (interruptable)
		{
			JPanel panel = new JPanel();
			add(panel, BorderLayout.SOUTH);
			JButton button = new JButton("Abbrechen");
			panel.add(button);
			button.addActionListener(interruptListener);
		}

		setUndecorated(true);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		pack();
		setResizable(false);
		setLocationRelativeTo(window);
	}


	public void setDescription(String text)
	{
		label.setText(text);
	}


	public void set(int value)
	{
		bar.setIndeterminate(false);
		bar.setValue(value);
		bar.setStringPainted(true);
	}
}
