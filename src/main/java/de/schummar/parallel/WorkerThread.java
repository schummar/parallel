package de.schummar.parallel;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;


class WorkerThread extends Progress
{
	private final Thread thread;

	private WorkerDialog dialog;

	private final double min = 0, max = 1;


	public WorkerThread(final Worker worker, final Window window, final boolean interruptable)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override public void run()
			{
				dialog = new WorkerDialog(window, interruptable, interruptListener);
				dialog.setVisible(true);
			}
		});


		thread = new Thread(new Runnable()
		{
			@Override public void run()
			{
				try
				{
					worker.work(WorkerThread.this);
				}
				catch (InterruptedException ex)
				{}
				catch (Exception ex)
				{}

				SwingUtilities.invokeLater(new Runnable()
				{
					@Override public void run()
					{
						dialog.setVisible(false);
					}
				});
			}
		});
		thread.start();
	}


	private final ActionListener interruptListener = new ActionListener()
	{
		@Override public void actionPerformed(ActionEvent e)
		{
			thread.interrupt();
		}
	};


	@Override public void setDescription(final String text)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override public void run()
			{
				dialog.setDescription(text);
			}
		});
	};


	@Override protected void valueChanged()
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override public void run()
			{
				dialog.set((int) ((min + (max - min) * get()) * 100));
			}
		});
	}
}