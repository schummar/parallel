package de.schummar.parallel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public abstract class Parallel
{
	// No input, no output //////////////////////////////////////////////////////
	public static void execute(int times, Progress progress, final Command command) throws InterruptedException
	{
		aExecute(times, null, progress, command);
	}


	public static void execute(int times, final Command command) throws InterruptedException
	{
		aExecute(times, null, null, command);
	}


	// Input, but no output //////////////////////////////////////////////////////
	public static <I> void execute(List<I> inputs, Progress progress, final CommandI<I> command) throws InterruptedException
	{
		aExecute(inputs.size(), inputs, progress, command);
	}


	public static <I> void execute(List<I> inputs, final CommandI<I> command) throws InterruptedException
	{
		aExecute(inputs.size(), inputs, null, command);
	}


	// No input, but output //////////////////////////////////////////////////////
	public static <O> List<O> execute(int times, Progress progress, final CommandO<O> command) throws InterruptedException
	{
		return aExecute(times, null, progress, command);
	}


	public static <O> List<O> execute(int times, final CommandO<O> command) throws InterruptedException
	{
		return aExecute(times, null, null, command);
	}


	// Input and output //////////////////////////////////////////////////////
	public static <I, O> List<O> execute(List<I> inputs, Progress progress, final CommandIO<I, O> command) throws InterruptedException
	{
		return aExecute(inputs.size(), inputs, progress, command);
	}


	public static <I, O> List<O> execute(List<I> inputs, final CommandIO<I, O> command) throws InterruptedException
	{
		return aExecute(inputs.size(), inputs, null, command);
	}


	private static <I, O> List<O> aExecute(final int times, final List<I> inputs, final Progress progress, final ACommand<I, O> command) throws InterruptedException
	{
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() - 1);
		final ObservableCounter counter = new ObservableCounter(0);
		final List<O> outputs = Collections.synchronizedList(new ArrayList<O>(inputs.size()));

		for (int i = 0; i < times; i++)
		{
			final int index = outputs.size();
			outputs.add(null);
			executor.execute(new Runnable()
			{
				@Override public void run()
				{
					try
					{
						outputs.set(index, command.aRun(inputs != null ? inputs.get(index) : null));
					}
					catch (InterruptedException e)
					{}

					double value = (double) counter.increment() / times;
					if (progress != null)
						progress.set(value);
				}
			});
		}
		try
		{
			counter.await(inputs.size());
		}
		catch (InterruptedException e)
		{
			throw e;
		}
		finally
		{
			executor.shutdownNow();
		}
		return outputs;
	}

	public static abstract class Command implements ACommand<Object, Object>
	{
		public abstract void run() throws InterruptedException;


		@Override public Object aRun(Object input) throws InterruptedException
		{
			run();
			return null;
		}
	}

	public static abstract class CommandI<I> implements ACommand<I, Object>
	{
		public abstract void run(I input) throws InterruptedException;


		@Override public Object aRun(I input) throws InterruptedException
		{
			run(input);
			return null;
		}
	}

	public static abstract class CommandO<O> implements ACommand<Object, O>
	{
		public abstract O run() throws InterruptedException;


		@Override public O aRun(Object input) throws InterruptedException
		{
			return run();
		}
	}

	public static abstract class CommandIO<I, O> implements ACommand<I, O>
	{
		public abstract O run(I input) throws InterruptedException;


		@Override public O aRun(I input) throws InterruptedException
		{
			return run(input);
		}
	}

	private static interface ACommand<I, O>
	{
		public O aRun(I input) throws InterruptedException;
	}
}
