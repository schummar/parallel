package de.schummar.parallel;

import java.util.ArrayList;
import java.util.List;

public abstract class Progress
{
	protected final List<ProgressPart> progressParts = new ArrayList<ProgressPart>();
	private double value = 0;


	public synchronized Progress split(double weight)
	{
		ProgressPart part = new ProgressPart(weight);
		progressParts.add(part);
		return part;
	}


	protected synchronized double get()
	{
		double values = 0, weights = 0;
		for (ProgressPart part : progressParts)
		{
			values += part.get() * part.getWeight();
			weights += part.getWeight();
		}
		return values + (1 - weights) * value;
	}


	public synchronized void set(double value)
	{
		this.value = value;
		valueChanged();
	}


	public abstract void setDescription(String text);


	protected abstract void valueChanged();

	private class ProgressPart extends Progress
	{
		private final double weight;


		public ProgressPart(double weight)
		{
			this.weight = weight;
		}


		public synchronized double getWeight()
		{
			return weight;
		}


		@Override public void setDescription(String text)
		{
			Progress.this.setDescription(text);
		}


		@Override protected void valueChanged()
		{
			Progress.this.valueChanged();
		}
	}
}
